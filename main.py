from data_transformation.DataFile import DataFile
from eviar.Fimed import Fimed
from evirs.ECFAR import ECFAR
import json


# Step 3: Evaluation
predictions = open("rules/prediction.txt", "r")
y = json.loads(predictions.read())
ratings = open("datasets/ratings.json", "r")
x = json.loads(ratings.read())
MAE = 0
N = 0
for user, predictions in y.items():
    for movie, rating in predictions.items():
        if movie not in x[user]:
            continue
        print(f"{user} gives {rating} ({x[user][movie]}) to movie {movie}")
        MAE += abs(int(rating) - int(x[user][movie]))
        N += 1
print("***********************************")
print(f"MAE = {MAE / N}")

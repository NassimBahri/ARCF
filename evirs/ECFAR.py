import random


class ECFAR:
    """
    Evidential Collaborative Filtering based on Association Rules.
    """

    def __init__(self, rules: list, domain: list) -> None:
        self.rules = []
        self.unclassified_instances = 0
        sorted(domain)
        domain = list(map(lambda x: str(x), domain))
        self.domain = domain
        for rule in rules:
            rule_consequent = rule["consequent"]
            focal_element = sorted((rule_consequent.split("#")[1]).split(","))
            if focal_element != domain:
                self.rules.append(rule)

    def predict(self, instances: dict):
        if len(self.rules) == 0:
            print("No rules found")
            return []
        classes = {}
        for user, instance in instances.items():
            classes[user] = {}
            k = random.choice(list(instance.keys()))
            fe = instance[k]
            while len(fe) == 1 and list(fe.keys())[0].split(",") == self.domain:
                k = random.choice(list(instance.keys()))
                fe = instance[k]
            classes[user][k] = self.classify(instance, str(k))
        return classes

    def classify(self, instance: dict, attribute: str) -> str:
        rules = []
        for rule in self.rules:
            if str(rule["consequent"].split("#")[0]) == attribute:
                rules.append(rule)
        print("The number of rules is : {}".format(len(rules)))
        instance_attributes = {}
        instance_data = {}
        instance_masses = {}
        instance_focal_elements = []
        original_class_value = instance[attribute]
        instance.pop(attribute)
        for attr in instance:
            instance_attributes[attr] = []
            instance_data[attr] = []
            instance_masses[attr] = []
            for focal_element in instance[attr]:
                instance_attributes[attr].append(attr + "#" + focal_element)
                instance_data[attr].append({"focal_element": focal_element, "mass": instance[attr][focal_element]})
                instance_masses[attr].append(instance[attr][focal_element])
                instance_focal_elements.append(attr + "#" + focal_element)
        i = 0
        while i < len(rules):
            rule_parts = rules[i]["antecedent"].split(";")
            rule_mass = 1
            total_intersections = 0
            for rule_part in rule_parts:
                attribute_mass = 0
                elements = rule_part.split("#")
                rule_focal_elements = set(elements[1].split(","))
                instance_values = []
                if elements[0] in instance_data:
                    instance_values = instance_data[elements[0]]
                for fe in instance_values:
                    values = set(fe["focal_element"].split(","))
                    intersection = len(values & rule_focal_elements)
                    union = len(values | rule_focal_elements)
                    if intersection > 0:
                        attribute_mass += fe["mass"] * intersection / union
                if attribute_mass > 0:
                    total_intersections += 1
                rule_mass *= attribute_mass
            if total_intersections == len(rule_parts):
                rules[i]["reliability"] = rule_mass
                i += 1
            else:
                del rules[i]
        if len(rules) == 0:
            self.unclassified_instances += 1
            return "0"
        bbas = []
        for rule in rules:
            contents = rule["consequent"].split("#")
            rule_class = contents[1].split(",")
            domain = self.domain.copy()
            domain = list(set(domain) - set(rule_class))
            other = ",".join(domain)
            rule_reliability = rule["confidence"] * rule["reliability"]
            other_reliability = (1 - rule["confidence"]) * rule["reliability"]
            bbas.append({
                ",".join(rule_class): round(rule_reliability, 2),
                other: round(other_reliability, 2),
                ",".join(self.domain): round(1 - rule_reliability - other_reliability, 2)
            })
        final_bba = bbas[0]
        lenght = len(bbas)
        for i in range(1, lenght):
            final_bba = ECFAR.dempster_combination(final_bba, bbas[i])
        values = {}
        for element in self.domain:
            values[element] = round(ECFAR.pignistic(set(element), final_bba), 3)
        values = sorted(values.items(), key=lambda item: item[1], reverse=True)
        return values[0][0]

    @staticmethod
    def dempster_combination(bba1, bba2):
        # bba format: {"1": 0.031, "2": 0.186, "1,3": 0.093, "1,2,3": 0.69}
        bba = {}
        # Extract focal elements
        focal_elements = set(bba1.keys())
        focal_elements = focal_elements.union(set(bba2.keys()))
        for focal_element in focal_elements:
            quotient = 0
            divisor = 0
            focal_element_items = set(focal_element.split(","))
            for fe1, mass1 in bba1.items():
                items1 = set(fe1.split(","))
                for fe2, mass2 in bba2.items():
                    items2 = set(fe2.split(","))
                    intersection = items1 & items2
                    if len(intersection) == 0:
                        divisor += mass1 * mass2
                    elif focal_element_items == intersection:
                        quotient += mass1 * mass2
            if divisor == 1:
                result = 0
            else:
                result = quotient / (1 - divisor)
            bba[focal_element] = round(result, 3)
        return bba

    @staticmethod
    def pignistic(focal_element, bba):
        pr = 0
        for fe, mass in bba.items():
            pr += len(focal_element & set(fe.split(","))) / len(set(fe.split(","))) * mass
        return pr
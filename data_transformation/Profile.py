class Profile:
    """
    A class used to represent a user profile in a voting system

    Methods
    -------
    zero_tolerance(rating)
        Generate soft rating for zero tolerance user
    tolerance_1(rating)
        Generate soft rating for (+/-1) tolerance user
    tolerance_1_end(rating)
        Generate soft rating for End-Weighted (+/-1) tolerance user
    intermediate_rating(probability, rating_values, proposition_max_size)
        Generate soft rating
    """

    @staticmethod
    def zero_tolerance(rating):
        """Generate soft rating for zero tolerance user

        Parameters
        ----------
        rating : int, [1, 2, 3, 4, 5]
            Five stars hard rating given by a user for a movie X
        """
        evi_rating = (rating, 1)
        return evi_rating

    @staticmethod
    def tolerance_1(rating):
        """Generate soft rating for (+/-1) tolerance user

        Parameters
        ----------
        rating : int, [1, 2, 3, 4, 5]
            Five stars hard rating given by a user for a movie X
        """
        if 1 < rating < 5:
            probability = 0.25
            proposition_max_size = 3
            rating_values = [rating, (rating - 1, rating), (rating, rating + 1), (rating - 1, rating, rating + 1)]
        else:
            probability = 0.5
            proposition_max_size = 2
            if rating == 1:
                rating_values = [rating, (rating, rating + 1)]
            else:
                rating_values = [rating, (rating - 1, rating)]
        return Profile.intermediate_rating(probability, rating_values, proposition_max_size)

    @staticmethod
    def tolerance_1_end(rating):
        """Generate soft rating for End-Weighted (+/-1) tolerance user

        Parameters
        ----------
        rating : int, [1, 2, 3, 4, 5]
            Five stars hard rating given by a user for a movie X
        """
        if 1 < rating < 5:
            probability = 0.25
            proposition_max_size = 3
            rating_values = [rating, (rating - 1, rating), (rating, rating + 1), (rating - 1, rating, rating + 1)]
            return Profile.intermediate_rating(probability, rating_values, proposition_max_size)
        else:
            evi_rating = (rating, 1)
            return evi_rating

    @staticmethod
    def intermediate_rating(probability, rating_values, proposition_max_size):
        """Generate soft rating

        Parameters
        ----------
        probability : float
            Probability associated to each proposition
        rating_values : List
            List of rating propositions
        proposition_max_size : int
            The maximum size of a proposition
        """
        uncertainty_values = []
        for rating in rating_values:
            if not isinstance(rating, int):
                uncertainty_values.append((len(rating) / proposition_max_size))
            else:
                uncertainty_values.append((1 / proposition_max_size))
        masses = []
        total = 0
        proposition_max_number = 2 ** proposition_max_size - 1
        rating_count = len(rating_values)
        for index, value in enumerate(uncertainty_values):
            if index < rating_count - 1:
                mass = round(((probability / value) * (rating_count / proposition_max_number)), 4)
                total += mass
                masses.append(mass)
            else:
                masses.append(round((1 - total), 4))
        return {
            "rating_values": rating_values,
            "masses": masses
        }
